/* terraform {
  required_version = ">= 1.2.9"
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.22.0"
    }
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.22.3"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.2.3"
    }
  }
}

provider "digitalocean" {
  token = var.DO_token
}

backend s3 {
    skip_credential_validation = true
    skip_metadata_api_check = true
    skip_region_validation = true
    endpoint = "https://sgp1.digtialoceanspaces.com"
    key = "states/terraform.tftate"
}

 */
#################
packer {
    required_plugins {
        digitalocean = {
            version = ">= 1.0.4"
            source = "github.com/digitalocean/digitalocean"
        }
    }
}